from django.shortcuts import render, redirect, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import ProductForm
from django.contrib.auth.decorators import login_required

# Create your views here.
from product_app.models import Product, Sells

@login_required
def home(request):
    """ products = Product.objects.all().order_by('-id')
    return render(request,
                  'product_app/index.html',
                  {'products': products}) """
    products = Product.objects.all().order_by('id')

    # Number of products to display per page
    items_per_page = 6
    paginator = Paginator(products, items_per_page)

    # Get the current page number from the request's GET parameters
    page = request.GET.get('page')

    try:
        # Get the Page object for the requested page
        products = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        products = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page of results
        products = paginator.page(paginator.num_pages)

    return render(request, 'product_app/index.html', {'products': products})

@login_required
def product_detail(request, product_id):
  product = Product.objects.get(id = product_id)
  sells_for_product = Sells.objects.filter(product=product)
  return render(request, 
                'product_app/product_detail.html',
                {'product': product, 'sells':sells_for_product})

@login_required
def search_products(request):
    query = request.GET.get('query', '')

    # Filter products based on the search query (adjust the field names as needed)
    products = Product.objects.filter(item_name__icontains=query)

    return render(request, 'product_app/search_results.html', {'query': query, 'products': products})

@login_required
def edit_product(request, product_id):
    # Get the product with the given ID or return a 404 response if not found
    product = get_object_or_404(Product, id=product_id)
    print(product)
    if request.method == 'POST':
        # Create a form instance and populate it with data from the request
        form = ProductForm(request.POST, instance=product)

        if form.is_valid():
            # Save the changes to the product
            form.save()
            return redirect('home')  # Redirect to the home page or another appropriate URL

    else:
        # Create a form instance and populate it with the current product data
        form = ProductForm(instance=product)
        print(product)
        return render(request, 'product_app/add_product.html', {'form':form,'product': product})

@login_required
def confirm_product(request, product_id):
    # Get the product with the given ID or return a 404 response if not found
    product = get_object_or_404(Product, id=product_id)

    # Update the product status to 'Confirmed'
    if product.status == 'Pending':
        product.status = 'Confirmed'
        product.save()

    return redirect('home')  # Redirect to the home page or another appropriate URL

@login_required
def delete_product(request, product_id):
    # Get the product with the given ID or return a 404 response if not found
    product = get_object_or_404(Product, id=product_id)

    if request.method == 'POST':
        # If the request method is POST, delete the product
         if product.status == 'Pending':
            product.delete()
            return redirect('home')  # Redirect to the home page or another appropriate URL
         else:
            # Show an error message if the product status is 'Confirmed'
            error_message = "You can't delete confirmed products."
            return render(request, 'product_app/delete_product.html', {'product': product, 'error_message': error_message})

    return render(request, 'product_app/delete_product.html', {'product': product})

@login_required
def add_product(request):
  # Assuming you have a form submitting data to this view
  if request.method == 'POST':
    # Get data from the form
    item_name = request.POST.get('item_name')
    total_quantity = int(request.POST.get('total_quantity', 0))
    unit_price = int(request.POST.get('unit_price', 0))

    # Create a new Product instance
    new_product = Product(
        item_name=item_name,
        total_quantity=total_quantity,
        unit_price=unit_price
    )

    # Save the new product to the database
    new_product.save()

    # Redirect to a success page or wherever you need to go
    return redirect('/home')

  # Render the form if it's a GET request
  return render(request, 'product_app/add_product.html')  # Adjust the template path as needed

@login_required
def sell_product(request, product_id):
    product = get_object_or_404(Product, id=product_id)

    if request.method == 'POST':
        patient_name = request.POST.get('patient_name')
        amount = int(request.POST.get('amount'))

        if product.total_quantity >= amount:
            # Create a new entry in the Sells table
            sell =Sells.objects.create(
                product=product,
                patient_name=patient_name,
                sell_amount=amount,
                total_price=amount*product.unit_price
            )
            # Update total_quantity and save
            product.total_quantity -= amount
            product.save()
            return render(request, 'product_app/sell_success.html', {'sell': sell, 'product': product})
        else:
            # Handle insufficient stock
            return render(request, 'product_app/sell_failure.html', {'message': 'Insufficient stock'})

    else:
        # Render the initial form
        return render(request, 'product_app/sell_product.html', {'product': product})