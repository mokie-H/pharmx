from django.db import models

# Create your models here.

class Product(models.Model):
    item_name = models.CharField(max_length = 50, null = True, blank = True)
    total_quantity = models.IntegerField(default = 0, null = True, blank = True)
    issued_quantity = models.IntegerField(default = 0, null = True, blank = True)
    received_quantity = models.IntegerField(default = 0, null = True, blank = True)
    unit_price = models.IntegerField(default = 0, null = True, blank = True)
    status = models.CharField(default='Pending', max_length = 10, blank = True)

    def __str__(self):
        return self.item_name


class Sells(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    patient_name = models.CharField(max_length=255)
    sell_amount = models.IntegerField()
    total_price = models.IntegerField(default = 0, null = True, blank = True)
    sell_date = models.DateTimeField(auto_now_add=True)