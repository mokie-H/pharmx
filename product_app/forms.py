from django import forms
from django.contrib.auth.forms import AuthenticationForm
from .models import Product

class CustomLoginForm(AuthenticationForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Your Username'}),
    )
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Your Password'}),
    )

class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['item_name', 'total_quantity', 'issued_quantity', 'received_quantity', 'unit_price']
