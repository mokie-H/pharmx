# Generated by Django 5.0 on 2024-01-12 06:26

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product_app', '0002_product_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sells',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('patient_name', models.CharField(max_length=255)),
                ('sell_amount', models.IntegerField()),
                ('total_price', models.IntegerField(blank=True, default=0, null=True)),
                ('sell_date', models.DateTimeField(auto_now_add=True)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='product_app.product')),
            ],
        ),
        migrations.DeleteModel(
            name='Sale',
        ),
    ]
