from django.urls import  path
from product_app import views
from django.contrib.auth import views as auth_views
from .forms import CustomLoginForm

urlpatterns = [
path('home/', views.home, name = "home"),
path('home/<int:product_id>/', views.product_detail, name='product_detail'),
path('edit/<int:product_id>/', views.edit_product, name='edit_product'),
path('sell/<int:product_id>/', views.sell_product, name='sell_product'),
path('search/', views.search_products, name='search_products'),
path('confirm/<int:product_id>/', views.confirm_product, name='confirm_product'),
path('delete/<int:product_id>/', views.delete_product, name='delete_product'),
path('add_product/', views.add_product, name='add_product'),
path('', auth_views.LoginView.as_view(template_name='product_app/login.html', authentication_form=CustomLoginForm), name='login'),
path('logout/',auth_views.LogoutView.as_view(template_name = 'product_app/logout.html'), name='logout'), 
]